package com.example.HibernateDemo.dao;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.HibernateDemo.entity.EmployeeEntity;

@Repository
public class EmployeeDao {

	@Autowired
	SessionFactory sessionFactory;
	
	public void saveEmployee(EmployeeEntity product){
		
		System.out.println("EmployeeEntity : : = "+product);
		System.out.println("sessionFactory : : = "+sessionFactory);
		//Session session = sessionFactory.getCurrentSession();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		System.out.println("Session object created : : = "+session);
		 session.save(product);
		 session.getTransaction().commit();
		 session.clear();
	}

}
