package com.example.HibernateDemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.HibernateDemo.dao.EmployeeDao;
import com.example.HibernateDemo.entity.AddressEntity;
import com.example.HibernateDemo.entity.EmployeeEntity;

@RestController
public class HibernateController {
	@Autowired
	EmployeeDao emp;
	
@RequestMapping("/")
public String addEmp(){

	
	AddressEntity address = new AddressEntity();
	address.setCity("Sarkaghat");
	address.setState("Himachal Pradesh");
	address.setCountry("India");
	
	
	EmployeeEntity empEntity = new EmployeeEntity();
	empEntity.setId(1);
	empEntity.setName("Senu");
	empEntity.setDept("Java");
	empEntity.setAddressEntity(address);
	
	
	
	/* Product product = new Product();
     product.setName("Civic");
     product.setDescription("Comfortable, fuel-saving car");
     product.setPrice(20000);
      
     // creates product detail
     ProductDetail detail = new ProductDetail();
     detail.setPartNumber("ABCDEFGHIJKL");
     detail.setDimension("2,5m x 1,4m x 1,2m");
     detail.setWeight(1000);
     detail.setManufacturer("Honda Automobile");
     detail.setOrigin("Japan");
      
     // sets the bi-directional association
    // product.setProductDetail(detail);
    detail.setProduct(product);
	*/
	
	
	
	emp.saveEmployee(empEntity);
	return "save successfully";
}

}
