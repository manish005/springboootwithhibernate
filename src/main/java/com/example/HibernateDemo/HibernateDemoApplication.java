package com.example.HibernateDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateDemoApplication {

/*	@Autowired
	EmployeeDao emp;*/
	
	public static void main(String[] args) {
		SpringApplication.run(HibernateDemoApplication.class, args);
		System.out.println("System started ...");
		//HibernateDemoApplication obj = new HibernateDemoApplication();
		//obj.addEmp();
		
	}
	
/*	public void addEmp(){
		EmployeeEntity empEntity = new EmployeeEntity();
		empEntity.setId(1);
		empEntity.setName("Manish");
		empEntity.setDept("Java");
		emp.saveEmployee(empEntity);
	}*/
}
